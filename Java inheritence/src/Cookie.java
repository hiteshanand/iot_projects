
public class Cookie extends DessertItem{
	protected int cost;
	int number;
	int price;
	double perprice;
	private static String cents2dollarsAndCents(int cents) {
		 
	    String s = "";
	     
	    if (cents < 0) {
	      s += "-";
	      cents *= -1;
	    }
	    
	    int dollars = cents/100;
	    cents = cents % 100;
	    
	    if (dollars > 0)
	      s += dollars;
	    
	    s +=".";
	      
	    if (cents < 10)
	      s += "0";
	      
	    s += cents;
	    return s;
	  }
	// Constructor
	public Cookie(String P_name, int P_number, int P_price) {
		name = String.valueOf(P_number) + " \t @ \t" + String.valueOf(cents2dollarsAndCents(P_price)) + "/dz  \n";
		name += P_name;
		number = P_number;
		price = P_price;
		perprice = price/12;
		cost = (int)Math.round(number*perprice);
	}
	public int getCost() {
		return cost;
	}

}
