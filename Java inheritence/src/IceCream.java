
public class IceCream extends DessertItem {
	protected int cost;
	// Constructor
	public IceCream(String P_name, int P_price) {
		name = P_name;
		cost = P_price;
	}
	
	public int getCost() {
		return cost;
	}
}
