/*
 * Name : 	Hitesh Anand
 * ID : 	80102488
 * Email : 	hanand@uncc.edu
 * 
 * This class defines and implements all the methods needed for TestCheckout and CheckoutGUI. 
 * It uses a vector to store all the various dessert items.
 * Each dessert is stored in a vector and is then processed to get the total tax and
 * the total cost which is the sum of the tax and the prices.
 * It then prints a reciept of the final values
 * 
 */
import java.util.*;
public class Checkout extends DessertShoppe {
	int temp_cost = 0;
	int taxtot = 0;
	Vector<DessertItem> v = new Vector<DessertItem>();
	public void enterItem(DessertItem des) {
		v.add(des);
	}
	public int numberOfItems() {
		return v.size();
	}
	public int totalCost() {
		temp_cost = 0;
		for(int i=0 ; i<v.size() ; i++) {
			temp_cost += v.get(i).getCost();
			
		}
		return temp_cost;
	}
	public int totalTax() {
		taxtot = 0;
		taxtot = (int)Math.round(temp_cost * TAX_RATE / 100);
		return taxtot;
	}
	
	public void clear() {
		v.clear();
	}
	
	  @Override
	  public String toString() {
		  temp_cost = 0;
		  for(int i=0 ; i<v.size() ; i++) {
				temp_cost += v.get(i).getCost();
				
			}
		  taxtot = 0;
			taxtot = (int)Math.round(temp_cost * TAX_RATE / 100);
		  String a ="\t\t" + STORE_NAME + "\n\t\t--------------------\n" ;
		  for(int i=0 ; i<v.size() ; i++) { 
			 a = a + v.get(i).getName() + "\t" + cents2dollarsAndCents(v.get(i).getCost()) + "\n"; 
		  }
		  a = a + "tax : \t\t" + cents2dollarsAndCents(taxtot) + "\n";
		  a = a + "Total Cost : \t\t" + cents2dollarsAndCents(temp_cost+taxtot) + "\n";
		  return a;
	  }
		  
	
}
