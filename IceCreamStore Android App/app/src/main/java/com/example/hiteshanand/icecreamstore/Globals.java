package com.example.hiteshanand.icecreamstore;

public class Globals {
    private static Globals instance;
    private static int flag = 0;
    private Globals(){}

    public static int getFlag() {
        return Globals.flag;
    }

    public static void setFlag(int flag) {
        Globals.flag = flag;
    }

   public static synchronized Globals getInstance(){
        if(instance==null){
            instance=new Globals();

        }
        return instance;
   }


}
