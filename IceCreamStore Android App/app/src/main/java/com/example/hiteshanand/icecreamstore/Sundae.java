package com.example.hiteshanand.icecreamstore;


public class Sundae extends IceCream {
    protected int cost;
    String name;
    int price;
    // Constructor
    public Sundae(String I_name, int I_price, String T_name, int T_price) {
        super(I_name,I_price);
        name = T_name;
        price = T_price;
        cost = super.getCost()+price;
    }

    public int getCost() {
        return cost;
    }
}
