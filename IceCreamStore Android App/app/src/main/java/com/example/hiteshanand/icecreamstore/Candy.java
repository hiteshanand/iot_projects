package com.example.hiteshanand.icecreamstore;

import android.widget.EditText;

public class Candy extends DessertItem {
    protected int cost;
    double weight;
    int price;

    private static String cents2dollarsAndCents(int cents) {

        String s = "";

        if (cents < 0) {
            s += "-";
            cents *= -1;
        }

        int dollars = cents/100;
        cents = cents % 100;

        if (dollars > 0)
            s += dollars;

        s +=".";

        if (cents < 10)
            s += "0";

        s += cents;
        return s;
    }

    public Candy(String P_name, double P_weight, int P_price) {
        name = String.valueOf(P_weight) + "lbs \t @ \t" + String.valueOf(cents2dollarsAndCents(P_price)) + "/lb  \n";
        name += P_name;
        weight=P_weight;
        price = P_price;
        cost = (int)Math.round(weight*price);
    }

    public int getCost() {
        return cost;
    }
}
