package com.example.hiteshanand.icecreamstore;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int flag=0;
    Checkout checkout = new Checkout();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void switchback_candy(View view){

        EditText can_name = findViewById(R.id.Candy_Name);
        String canname = can_name.getText().toString();


        EditText can_weight = findViewById(R.id.Candy_Weight);
        double canweight = 0;
        try{
            canweight = Double.parseDouble(can_weight.getText().toString());
        }catch(Exception e){

        }


        EditText can_cost = findViewById(R.id.Candy_Cost);
        int cancost=0;
        try{
            cancost = Integer.parseInt(can_cost.getText().toString());
        }catch(Exception e){

        }
        checkout.enterItem(new Candy(canname, canweight, cancost));
        setContentView(R.layout.activity_main);
    }
    public void switchback_cok(View view){
        EditText cok_name = findViewById(R.id.Cookie_name);
        String cokname = cok_name.getText().toString();


        EditText can_weight = findViewById(R.id.Cookie_num);
        int coknum = 0;
        try{
            coknum = Integer.parseInt(can_weight.getText().toString());
        }catch(Exception e){

        }


        EditText can_cost = findViewById(R.id.Cookie_cost);
        int cokcost=0;
        try{
            cokcost = Integer.parseInt(can_cost.getText().toString());
        }catch(Exception e){

        }
        checkout.enterItem(new Cookie(cokname, coknum, cokcost));
        setContentView(R.layout.activity_main);
    }
    public void switchback_ice(View view) {
        EditText name = findViewById(R.id.Ice_Name);
        String icename = name.getText().toString();


        EditText can_cost = findViewById(R.id.Ice_Cost);
        int icecost=0;
        try{
            icecost = Integer.parseInt(can_cost.getText().toString());
        }catch(Exception e){

        }
        checkout.enterItem(new IceCream(icename, icecost));
        setContentView(R.layout.activity_main);
    }
    public void switchback_sun(View view){
        EditText sun_name = findViewById(R.id.Sundae_Name);
        String sunname = sun_name.getText().toString();


        EditText can_weight = findViewById(R.id.Sundae_Cost);
        int suncost = 0;
        try{
            suncost = Integer.parseInt(can_weight.getText().toString());
        }catch(Exception e){

        }

        EditText top_name = findViewById(R.id.Sundae_Topping_name);
        String topname = sun_name.getText().toString();

        EditText can_cost = findViewById(R.id.Sundae_topping_cost);
        int topcost=0;
        try{
            topcost = Integer.parseInt(can_cost.getText().toString());
        }catch(Exception e){

        }
        checkout.enterItem(new Sundae(sunname,suncost,topname,topcost));
        setContentView(R.layout.activity_main);
    }

    public void switch_candy(View view){
        setContentView(R.layout.activity_candy);
    }
    public void switch_cookie(View view){
        setContentView(R.layout.activity_cookie_activity);
    }
    public void switch_ice(View view){
        setContentView(R.layout.activity_ice_cream_);
    }
    public void switch_sundae(View view){
        setContentView(R.layout.activity_sundae_);
    }
    public void clear_button(View view){
        checkout.clear();
        TextView textView = findViewById(R.id.reciept);
        textView.setText(" ");
    }

    public void receipt_button(View view) {
        TextView textView = findViewById(R.id.reciept);
        textView.setText(checkout.opt());
    }
}




