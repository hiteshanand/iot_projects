/* 
 * Name : Hitesh Anand
 * Email ID : hanand@uncc.edu
 * 800 number : 801024848
 * 
 * This is a simple program which reads information from a file which has over
 * a hundred entries of different bikes. These entries are then analyzed to 
 * ensure they meet the criteria which was entered in the command line. 
 * If they don't meet the criteria, they are removed from the ArrayList. 
 * This ArrayList is then sorted according to the last parameter in 
 * the command line and is printed on the console
 * 
 */

import java.io.*;
import java.util.*;

public class ShowBikes {

	public static void main(String[] Args) {
		// Flags and the values to be compared to which will be used later are declared here. 
		int typeflag = 0;
		int gearflag = 0, gearval = 0;
		int wheelflag = 0, wheelval = 0;
		int heightflag = 0, heightval = 0;
		int colorflag = 0,consflag = 0;
		String colorval = null, typeval = null, consval = null;
		
		// Scanner to read file
		Scanner x=null;
		int command_length = Args.length;
		
		if(command_length==0) {
			System.out.println("Enter parameters");
			return;
		}
		
		// Sort by which parameter is found here
		String srt = Args[command_length-1];
		
		// Set the flags and their values
		for(int i=0;i<command_length-1;i=i+2) {
			if(Args[i].equalsIgnoreCase("-type")) {
				typeflag = 1;
				typeval = Args[i+1];
				if(typeval.equals("mountain_bike") == false && typeval.equals("race_bike")== false && typeval.equals("street_bike")==false) {
					System.out.println("Wrong type entered");
					return;
				}
			}
			else if(Args[i].equalsIgnoreCase("-gear")) {
				gearflag = 1;
				try {
					gearval = Integer.parseInt(Args[i+1]);
				}catch(Exception e) {
					System.out.println("Invalid Value for gear");
					return;
				}
				gearval = Integer.parseInt(Args[i+1]);
				if(gearval<4 ||gearval>10) {
					System.out.println("Invalid Value for gear");
					return;
				}
			}
			else if(Args[i].equalsIgnoreCase("-wheel")) {
				wheelflag = 1;
				try {
					wheelval = Integer.parseInt(Args[i+1]);
				}catch(Exception e) {
					System.out.println("Invalid value for wheel");
					return;
				}
				
				if(wheelval<36 || wheelval>60 || wheelval%6 !=0) {
						System.out.println("Invalid Value for wheel");
						return;
					
				}
					
			}
			else if(Args[i].equalsIgnoreCase("-height")) {
				heightflag = 1;
				try {
				heightval = Integer.parseInt(Args[i+1]);
				}catch (Exception e) {
					System.out.println("Invalid Value for Height");
					return;
				}
				if(heightval<4 ||heightval>10) {
					System.out.println("Invalid Value for height");
					return;
				}
			}
			else if(Args[i].equalsIgnoreCase("-color")) {
				colorflag = 1;
				colorval = Args[i+1];
				if(colorval.equals("steel") == false && colorval.equals("red") == false && colorval.equals("blue") == false && colorval.equals("black") == false) {
					System.out.println("Wrong color entered");
					return;
				}
			}
			else if(Args[i].equalsIgnoreCase("-cons")) {
				consflag = 1;
				consval = Args[i+1];
				if(consval.equals("carbon") == false && consval.equals("aluminium") == false && consval.equals("steel") == false) {
					System.out.println("Wrong construction material entered");
					return;
				}
			}
			else {
				System.out.println("Invalid Argument entered");
				return;
			}
		}
				
		/* Code Snippet to check files inside current directory 
		 *File file = new File(".");
		 *for(String fileNames : file.list()) System.out.println(fileNames);
		 */ 
		 
		// Open File to read Data using try catch exception. Handle file missing exception
		try {
			x = new Scanner(new File("hw2.txt"));
		}catch(Exception e) {
			System.out.println("Couldnt find file");
		}
		
		//Array list which will contain all bikes
		ArrayList<bikes> bks = new ArrayList<bikes>();
		
		// Read data from file until you reach the end of the file
		while(x.hasNext()) {
			String type = x.next();
			int gear =x.nextInt();
			int wheel = x.nextInt();
			int height = x.nextInt();
			String color = x.next();
			String cons = x.next();
			bikes bke = new bikes(type,gear,wheel,height,color,cons);
			bks.add(bke);
			
		}
		
		/*
		 *  Go through each element in the array list and ensure it meets
		 * if it doesnt, remove it from the array list
		 * 
		 */
		for(int i=0;i<bks.size();i++) {
			if(typeflag == 1) {
				if(bks.get(i).type.equalsIgnoreCase(typeval)==false) {
					bks.remove(i);
					i--;
					continue;
				}
			}
			if(colorflag == 1) {
				if(bks.get(i).color.equalsIgnoreCase(colorval)==false) {
					bks.remove(i);
					i--;
					continue;
				}
			}
			if(consflag == 1) {
				if(bks.get(i).cons.equalsIgnoreCase(consval)==false) {
					bks.remove(i);
					i--;
					continue;
				}
			}
			if(gearflag == 1) {
				if(bks.get(i).gear != gearval) {
					bks.remove(i);
					i--;
					continue;
				}
			}
			if(wheelflag == 1) {
				if(bks.get(i).wheel != wheelval) {
					bks.remove(i);
					i--;
					continue;
				}
			}
			if(heightflag == 1) {
				if(bks.get(i).height != heightval) {
					bks.remove(i);
					i--;
					continue;
				}
			}
		}
		
		// Sort the list based on the parameter
		
		if(srt.equalsIgnoreCase("-gear")) {
			Collections.sort(bks,new Comparator<bikes>() {
				@Override
				public int compare(bikes e1, bikes e2)
				{	
					int gear1 = e1.gear;
					int gear2 = e2.gear;
					return  (gear1-gear2);
				}
				
			});
		}
		else if(srt.equalsIgnoreCase("-wheel")) {
			Collections.sort(bks,new Comparator<bikes>() {
				@Override
				public int compare(bikes e1, bikes e2)
				{	
					int wheel1 = e1.wheel;
					int wheel2 = e2.wheel;
					return (wheel1-wheel2);
				}
				
			});
		}
		else if(srt.equalsIgnoreCase("-height")) {
			Collections.sort(bks,new Comparator<bikes>() {
				@Override
				public int compare(bikes e1, bikes e2)
				{	
					int height1 = e1.height;
					int height2 = e2.height;
					return  (int) (height1-height2);
				}
				
			});
		}
		else {
			System.out.println("Wrong sort value");
			return;
		}
			
		
		// Print out the sorted list.
		System.out.println("\nSorted ArrayList");
        for (int i=0; i<bks.size(); i++)
            System.out.println("\n\nType : " + bks.get(i).type + "\nGear : "+ bks.get(i).gear  + "\nWheel Size : "+ bks.get(i).wheel + "\nHeight : "+ bks.get(i).height + "\nColor : "+ bks.get(i).color + "\nConstruction Medium : "+ bks.get(i).cons);
		

	}

}
